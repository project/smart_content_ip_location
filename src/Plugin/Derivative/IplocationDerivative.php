<?php

namespace Drupal\smart_content_ip_location\Plugin\Derivative;

use Drupal\Component\Plugin\Derivative\DeriverBase;

class IplocationDerivative extends DeriverBase {

  /**
   * @inheritdoc
   */
  public function getDerivativeDefinitions($base_plugin_definition) {
    $this->derivatives = [
      'country' => [
          'label' => 'Country Name',
          'type' => 'textfield',
        ] + $base_plugin_definition,      
     'country_code' => [
          'label' => 'Country Code',
          'type' => 'textfield',
        ] + $base_plugin_definition,      
     'city' => [
          'label' => 'City',
          'type' => 'textfield',
        ] + $base_plugin_definition,      
     'continent' => [
          'label' => 'Continent',
          'type' => 'textfield',
        ] + $base_plugin_definition,      
     'region' => [
          'label' => 'Region',
          'type' => 'textfield',
        ] + $base_plugin_definition,      
     'latitude' => [
          'label' => 'Latitude',
          'type' => 'textfield',
        ] + $base_plugin_definition,      
     'longitude' => [
          'label' => 'Longitude',
          'type' => 'textfield',
        ] + $base_plugin_definition,      
    ];
    return $this->derivatives;
  }

}
