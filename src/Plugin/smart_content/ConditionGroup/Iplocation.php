<?php

namespace Drupal\smart_content_ip_location\Plugin\smart_content\ConditionGroup;

use Drupal\smart_content\ConditionGroup\ConditionGroupBase;

/**
 * Provides a default Smart Condition.
 *
 * @SmartConditionGroup(
 *   id = "iplocation",
 *   label = @Translation("IP Location"),
 *   weight = 0,
 * )
 */
class Iplocation extends ConditionGroupBase {

}
