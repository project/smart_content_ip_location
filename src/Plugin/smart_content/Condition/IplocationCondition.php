<?php

namespace Drupal\smart_content_ip_location\Plugin\smart_content\Condition;

use Drupal\smart_content\Condition\ConditionTypeConfigurableBase;

/**
 * Provides a default Smart Condition.
 *
 * @SmartCondition(
 *   id = "iplocation",
 *   label = @Translation("IP Location"),
 *   group = "iplocation",
 *   weight = 0,
 *   deriver = "Drupal\smart_content_ip_location\Plugin\Derivative\IplocationDerivative"
 * )
 */
class IplocationCondition extends ConditionTypeConfigurableBase {

  /**
   * @inheritdoc
   */
  public function getLibraries() {
    $libraries = array_unique(array_merge(parent::getLibraries(), ['smart_content_ip_location/condition.iplocation']));
    return $libraries;
  }

}