(function ($) {

  Drupal.smart_content.SmartContentManager.plugin = Drupal.smart_content.SmartContentManager.plugin || {};
  Drupal.smart_content.SmartContentManager.plugin.Field = Drupal.smart_content.SmartContentManager.plugin.Field || {};
  Drupal.smart_content.SmartContentManager.plugin.Field.iplocationSmartCondition = {
    init: function (Field) { var location_data = drupalSettings.smart_content_ip_location.iplocationJS.location_data; 
	
      if(Field.pluginId == 'iplocation:country') {
        Field.claim();
		Field.complete(location_data.country);
	  }
      else if(Field.pluginId == 'iplocation:country_code') {
        Field.claim();
        Field.complete(location_data.countryCode); 

      }
	  else if(Field.pluginId == 'iplocation:city') {
        Field.claim();
        Field.complete(location_data.city);

      }
	  else if(Field.pluginId == 'iplocation:continent') {
        Field.claim();
        Field.complete(location_data.continent);

      }
	  else if(Field.pluginId == 'iplocation:region') {
        Field.claim();
        Field.complete(location_data.region);

      }
	  else if(Field.pluginId == 'iplocation:latitude') {
        Field.claim();
        Field.complete(location_data.lat);

      }
	  else if(Field.pluginId == 'iplocation:longitude') {
        Field.claim();
        Field.complete(location_data.lon);

      }
        
    }
  }
  
    
})(jQuery);