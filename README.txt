INTRODUCTION
------------

Smart Content IP Location is a connector module to allow geo location fields available through Extreme-IP-Lookup web API to be used as
conditions in Smart Content.

 * For a full description of the module, visit the project page:
   https://www.drupal.org/project/smart_content_ip_location

 * To submit bug reports, feature suggestions or to track changes:
   https://www.drupal.org/project/issues/smart_content_ip_location


REQUIREMENTS
------------

This module requires the following modules:

 * Smart Content (https://www.drupal.org/project/smart_content)


INSTALLATION
------------

This module does not require composer to install. Please follow standard process to install


CONFIGURATION
-------------

This module does not require any configuration. 


MAINTAINERS
-----------

Current maintainers:
 * Nitin Tatte (michaellander) - https://www.drupal.org/u/michaellander
